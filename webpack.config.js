const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');

const clientConfig = {
  entry: {
    main: './src/index.js',
  },
  target: 'web',
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'dist'),
  },
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    http2: true,
    compress: true,
    port: 4040,
    host: '0.0.0.0',
  },
  plugins: [
    new CopyPlugin([
      { from: path.join(__dirname, 'src/maps'), to: path.join(__dirname, 'dist/maps'), },
      { from: path.join(__dirname, 'src/assets'), to: path.join(__dirname, 'dist/assets'), },
      { from: path.join(__dirname, 'src/static'), to: path.join(__dirname, 'dist'), },
    ]),
  ],
};

module.exports = clientConfig;