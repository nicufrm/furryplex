export const DEV = false;
export const DESIGN_MODE = false;
export const DOWNLOAD = true;
export const WIDTH = 800;
export const HEIGHT = 600;
export const SCALE = 1;
export const CELL_SIZE = 60;
export const BORDER_WIDTH = 4;
export const MARGIN_SIZE = 20;
export const HUD_HEIGHT = 60 + BORDER_WIDTH;
export const MAP_SIZE = {X: 50, Y: 50};
export const MOVE_SPEED = 6;
export const FALL_DELAY = 50;
export const PUSH_DELAY = 150;
export const REWIND_DELAY = 500;
export const REWIND_DELAY_REPEAT = 150;
export const MAX_FPS = 80;

export const TYPES = {
  OCCUP: -1,
  EMPTY: 0,
  FURRR: 1,
  GRASS: 2,
  BALLL: 3,
  MAZEE: 6,
  APPLE: 7,
  RNBOW: 8,
  BRICK: 9,
  SBRCK: 10,
  HEART: 11,
  EXITT: 12,
}

export const SYMBOLS = {
  'X': TYPES.OCCUP,
  ' ': TYPES.EMPTY,
  'F': TYPES.FURRR,
  'G': TYPES.GRASS,
  'O': TYPES.BALLL,
  '.': TYPES.MAZEE,
  'A': TYPES.APPLE,
  'R': TYPES.RNBOW,
  '-': TYPES.BRICK,
  '+': TYPES.SBRCK,
  '@': TYPES.HEART,
  'E': TYPES.EXITT,
}

export const SYMBOLS_REV = Object.fromEntries(Object.entries(SYMBOLS).map(line => line.reverse()))

export const CONTROLS = {
  'ArrowUp': 'up',
  'ArrowDown': 'down',
  'ArrowLeft': 'left',
  'ArrowRight': 'right',
  'w': 'up',
  'a': 'down',
  's': 'left',
  'd': 'right',
  'W': 'up',
  'A': 'down',
  'S': 'left',
  'D': 'right',
  'Escape': 'reset',
  ' ': 'rewind',
  '+': 'zoomin',
  '=': 'zoomin',
  '-': 'zoomout',
  '_': 'zoomout',
  'N': 'newmap',
  'n': 'newmap',
  'G': 'gravity',
  'g': 'gravity',
}