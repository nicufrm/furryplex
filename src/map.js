import { TYPES as T, SYMBOLS} from './consts.js';
import { random } from './random.js'
import * as CONST from './consts.js';

export const setMapItem = (mapData, sym, {x, y}) => {
  const newMapData = mapData.map(line => line.split(''))
  newMapData[y][x] = sym
  return newMapData.map(line => line.join(''))
}

export const generateMap = (width, height, furpos, exitpos) => {
  let mapData = Array(height).fill(' '.repeat(width));


  mapData = generateMaze(width, height)
  return mapData

    // const pool =
  //   '                                                                              '+
  //   '                                                                              '+
  //   '                                                                              '+
  //   '                                                                              '+
  //   '..............................................................................'+
  //   '..............................................................................'+
  //   '..............................................................................'+
  //   '..............................................................................'+
  //   'OOOOOOOOOO'+
  //   'OOOOOOOOOO'+
  //   'OOOOOOOOOO'+
  //   'OOOOOOOOOO'+
  //   'AAAAAAAAAA'+
  //   'R' +
  //   '+++++++' +
  //   '+++++++' +
  //   '-------------------------' +
  //   '-------------------------' +
  //   '-------------------------' +
  //   '@@@@@@'

  const pool =
    '  '+
    '..........................................................................................'+
    'OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO'+
    'AAAA'+
    '----' +
    '+++++++++++++++++++' +
    'R'
    .split();

  mapData = mapData.map(  line => line.split('').map( sym => pool[random.rand(pool.length)] ).join('') )
  if(furpos) mapData = setMapItem(mapData, 'F', furpos)
  if(exitpos) mapData = setMapItem(mapData, 'E', exitpos)
  return mapData
}


const generateMaze = (width, height) => {
  const cellWidth = [1,3,7][random.rand(3)]
  const cellHeight = [1,3,7,15][random.rand(3)]
  const wall = '-'
  const fill = '.'
  const gate = ['O','.'][random.rand(2)]
  const centerGate = '.'

  const centerOffsetX = Math.floor((cellWidth - 1) / 2) + 1;
  const centerOffsetY = Math.floor((cellHeight - 1) / 2) + 1;
  const mapData = Array(height).fill('').map(line => Array(width).fill(wall))
  const mazeWidth = Math.floor((width - 1) / (cellWidth + 1))
  const mazeHeight = Math.floor((height - 1) / (cellHeight + 1))
  const {cells, exit, start, deadEnds} = newMaze( mazeWidth, mazeHeight)

  cells.forEach((mazeLine, lineIndex) => mazeLine.forEach((mazeCell, cellIndex) => {
    const line = lineIndex * (cellHeight + 1) + 1
    const col = cellIndex * (cellWidth + 1) + 1

    for (let x = 0; x < cellWidth; x++) {
      for (let y = 0; y < cellHeight; y++) {
        mapData[line+y][col+x] = fill
      }
    }
    if (mazeCell[0] === 1) {
      for (let x = 0; x < cellWidth; x++) {
        if(x == centerOffsetX - 1) {
          mapData[line - 1][col + x] = centerGate
        } else {
          mapData[line - 1][col + x] = gate
        }
      }
    }
    if (mazeCell[3] === 1) {
      for (let y = 0; y < cellHeight; y++) {
        if(y == centerOffsetY - 1) {
          mapData[line + y][col - 1] = centerGate
        } else {
          mapData[line + y][col - 1] = gate
        }
      }
    }

  }))

  deadEnds.forEach(deadEnd => {
    mapData[deadEnd[0] * (cellHeight + 1) + centerOffsetY][deadEnd[1] * (cellWidth + 1) + centerOffsetX] = 'A'
  })

  mapData[start[0] * (cellHeight + 1) + centerOffsetY][start[1] * (cellWidth + 1) + centerOffsetX] = 'F'
  mapData[exit[0] * (cellHeight + 1) + centerOffsetY][exit[1] * (cellWidth + 1) + centerOffsetX] = 'E'

  return mapData.map( line => line.join('') )
}

export const parseMap = (mapData) => {
  const cells = mapData.map((line => line.split('').map(sym => SYMBOLS[sym])))
  return { cells };
}

export class MapHistory {
  constructor(){
    this.history = [];
  }

  pushCompiled(compiledMap) {
    this.history.push(compiledMap)
  }

  push(mapData) {
    const compiledMap = mapData.map( line => line.map( cell => cell && cell.type || CONST.TYPES.EMPTY ) )
    this.history.push(compiledMap)
  }

  pop() {
    if (this.history.length > 2)
      return this.history.pop()
    else
      return this.history[0] || null
  }
}


const newMaze = (x, y, currentCell) => {

  // Establish variables and starting grid
  var totalCells = x*y;
  var cells = new Array();
  var unvis = new Array();
  var next, start, exit, deadEnds = [], longestLine = 0, goingForward = true;
  for (var i = 0; i < y; i++) {
      cells[i] = new Array();
      unvis[i] = new Array();
      for (var j = 0; j < x; j++) {
          cells[i][j] = [0,0,0,0];
          unvis[i][j] = true;
      }
  }

  // Set a random position to start from
  currentCell = currentCell || [Math.floor(Math.random()*y), Math.floor(Math.random()*x)];
  start = currentCell
  var path = [currentCell];
  unvis[currentCell[0]][currentCell[1]] = false;
  var visited = 1;

  // Loop through all available cell positions
  while (visited < totalCells) {
      // Determine neighboring cells
      var pot = [[currentCell[0]-1, currentCell[1], 0, 2],
              [currentCell[0], currentCell[1]+1, 1, 3],
              [currentCell[0]+1, currentCell[1], 2, 0],
              [currentCell[0], currentCell[1]-1, 3, 1]];
      var neighbors = new Array();

      // Determine if each neighboring cell is in game grid, and whether it has already been checked
      for (var l = 0; l < 4; l++) {
          if (pot[l][0] > -1 && pot[l][0] < y && pot[l][1] > -1 && pot[l][1] < x && unvis[pot[l][0]][pot[l][1]]) { neighbors.push(pot[l]); }
      }

      // If at least one active neighboring cell has been found
      if (neighbors.length) {
          // Choose one of the neighbors at random
          next = neighbors[Math.floor(Math.random()*neighbors.length)];

          // Remove the wall between the current cell and the chosen neighboring cell
          cells[currentCell[0]][currentCell[1]][next[2]] = 1;
          cells[next[0]][next[1]][next[3]] = 1;

          // Mark the neighbor as visited, and set it as the current cell
          unvis[next[0]][next[1]] = false;
          visited++;
          currentCell = [next[0], next[1]];
          path.push(currentCell);
          goingForward = true
      }
      // Otherwise go back up a step and keep going
      else {
          if(goingForward) deadEnds.push(currentCell)
          currentCell = path.pop();
          goingForward = false
      }

      if(path.length > longestLine){
        longestLine = path.length
        exit = currentCell
      }
  }
  return {cells, start, exit, deadEnds};
}