export class KeyStack {
  constructor(controls, onUpdate) {
    this.keyList = Object.keys(controls);
    this.controls = controls;
    this.onUpdate = onUpdate;
    this.stack = [];
    this.addEventListeners();
  }

  addEventListeners() {
    window.addEventListener("keydown", this.downListener.bind(this), false);
    window.addEventListener("keyup", this.upListener.bind(this), false);
  }

  downListener(event) {
    const key = event.key;
    if (this.keyList.includes(key)) {
      this.stack = this.stack.filter(stackKey => stackKey != key)
      this.stack.unshift(key)
      this.update()
    }
  }

  upListener(event) {
    const key = event.key;
    if (this.keyList.includes(key)) {
      this.stack = this.stack.filter(stackKey => stackKey != key)
      this.update()
    }
  }

  update() {
    if (this.onUpdate) {
      const mappedStack = this.stack.map( key => this.controls[key])
      this.onUpdate(mappedStack[0] || null, mappedStack)
    }
  }
}