import * as CONST from './consts.js';

export function calcPosition({x, y}) {
  return {
    x: x * CONST.CELL_SIZE,
    y: y *CONST.CELL_SIZE,
  }
}

export function download(filename, text) {
  var element = document.createElement('a');
  element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
  element.setAttribute('download', filename);

  element.style.display = 'none';
  document.body.appendChild(element);

  element.click();

  document.body.removeChild(element);
}