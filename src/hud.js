import * as PIXI from 'pixi.js-legacy';
import { random } from './random.js'
import * as CONST from './consts.js';
import { appSingleton } from './appsingleton.js'

export class Hud {
  constructor(){
    this.app = appSingleton.app
    this.textContent = 'Level 5689    ->   Hp:2     ->      Apples: 23'
    this.hudBorderWidth = CONST.BORDER_WIDTH;
  }
  create() {
    const hudBorderWidth = CONST.BORDER_WIDTH;
    this.hudBorder = new PIXI.NineSlicePlane(this.app.resources.border2.texture, hudBorderWidth + 1, hudBorderWidth + 1, hudBorderWidth + 1, hudBorderWidth + 1);
    this.hudBorder.zIndex = 0;

    this.hudBackground = new PIXI.TilingSprite(this.app.resources.noisebg.texture, this.hudBorder.width - 2*hudBorderWidth, this.hudBorder.height - 2*hudBorderWidth);
    this.hudBackground.zIndex = 1;


    this.hearts = [];

    const style = new PIXI.TextStyle({
      dropShadow: true,
      dropShadowDistance: 2,
      fill: [
          "#d0d6dd",
          "#989898"
      ],
      fontFamily: "Press Start 2P",
      fontSize: 16
    });

    this.textSprite = new PIXI.Text(this.textContent, style);
    this.textSprite.zIndex = 10;

    this.hudContainer = new PIXI.Container();

    this.app.pixiApp.stage.addChild(this.hudContainer);
    this.resize();

    this.hudContainer.addChild(this.hudBorder);
    this.hudContainer.addChild(this.hudBackground);
    this.hudContainer.addChild(this.textSprite);
    this.hudContainer.sortChildren();
  }

  resize() {
    this.hudBorder.x = 0
    this.hudBorder.y = 0
    this.hudBorder.width = this.app.width - 2*(CONST.MARGIN_SIZE - this.hudBorderWidth)
    this.hudBorder.height = CONST.HUD_HEIGHT - (CONST.MARGIN_SIZE - this.hudBorderWidth)
    this.hudBackground.x = this.hudBorderWidth;
    this.hudBackground.y = this.hudBorderWidth;
    this.hudBackground.width = this.hudBorder.width - 2*this.hudBorderWidth;
    this.hudBackground.height = this.hudBorder.height - 2*this.hudBorderWidth;
    this.textSprite.x = this.hudBorderWidth + 10;
    this.textSprite.y = this.hudBorderWidth + 12;
    this.hudContainer.x = CONST.MARGIN_SIZE - this.hudBorderWidth
    this.hudContainer.y = CONST.MARGIN_SIZE - this.hudBorderWidth
  }

  setText(text='') {
    let newText = text;
    const locations = [...newText.matchAll(/@/g)];

    this.removeHearts()
    locations.forEach(location => {
      newText = newText.substr(0, location.index) + ' ' + newText.substr(location.index + 1);
      this.renderHeart(location.index)
    });

    this.textContent = newText
    this.textSprite && (this.textSprite.text = this.textContent)
  }

  renderHeart(index){
    const SIZE = 16;

    const heart = new PIXI.Sprite(this.app.resources.smallheart.texture);
    heart.x = this.textSprite.x + index * SIZE;
    heart.y = this.textSprite.y;
    heart.anchor.set(0);
    heart.zIndex = 20;

    this.hudContainer.addChild(heart);
    this.hearts.push(heart)

  }

  removeHearts(){
    this.hearts.forEach(heart => {
      this.hudContainer.removeChild(heart);
      heart.destroy()
    })
    this.hearts=[]
  }
}