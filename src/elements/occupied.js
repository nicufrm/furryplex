
import * as CONST from '../consts.js';
import { Element } from './templates/element.js'

export class Occupied extends Element{
  static get type() {
    return CONST.TYPES.OCCUP
  }

  static get assets() {
    return [
      {name: 'placeholder', path: 'assets/placeholder/placeholder.json'},
    ];
  }

  constructor(...args) {
    super(...args);

    Object.assign(this, {
      slippery: false,
      eddible: false,
    });
  }
}