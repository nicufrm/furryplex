import * as PIXI from 'pixi.js-legacy';
import { appSingleton } from '../appsingleton.js'
import { random } from '../random.js'
import * as CONST from '../consts.js';
import { Element } from './templates/element.js'

export class Maze extends Element{
  static get type() {
    return CONST.TYPES.MAZEE
  }

  static get assets() {
    return [
      {name: 'maze', path: 'assets/maze2/maze2.json'},
    ];
  }

  constructor(...args) {
    super(...args);

    Object.assign(this, {
      texture: 'maze',
      eddible: true,
      borderTopFrame: 7,
      cornerTopRightFrame: 8,
      fillsSpace: true,
      frame: random.rand(7)
    });
  }

  animate({type = 'init'} = {}) {
    if (type === 'init' || type === 'surroundings-changed'){
      const app = appSingleton.app;

      const left = app.getMap(this.position, 'left');
      const right = app.getMap(this.position, 'right');
      const up = app.getMap(this.position, 'up');
      const down = app.getMap(this.position, 'down');
      const leftMAZE = left === null || left && !left.fillsSpace;
      const rightMAZE = right === null || right && !right.fillsSpace;
      const upMAZE = up === null || up && !up.fillsSpace;
      const downMAZE = down === null || down && !down.fillsSpace;


      const newhash = ('' + leftMAZE+rightMAZE+upMAZE+downMAZE)

      if(this.lastSurroundings !== newhash){
        this.lastSurroundings = newhash

        this.sprite.cacheAsBitmap = false;

        while(this.sprite.children[0]) {
          this.sprite.removeChild(this.sprite.children[0]);
        }

        const borderTopTexture = app.getAnimation(this.texture)[this.borderTopFrame];
        const cornerTopRightTexture = app.getAnimation(this.texture)[this.cornerTopRightFrame];

        if(leftMAZE) this.makeRotatedSprite(borderTopTexture, 270);
        if(rightMAZE) this.makeRotatedSprite(borderTopTexture, 90);
        if(upMAZE) this.makeRotatedSprite(borderTopTexture, 0);
        if(downMAZE) this.makeRotatedSprite(borderTopTexture, 180);

        if(upMAZE && rightMAZE) this.makeRotatedSprite(cornerTopRightTexture, 0);
        if(downMAZE && rightMAZE) this.makeRotatedSprite(cornerTopRightTexture, 90);
        if(downMAZE && leftMAZE) this.makeRotatedSprite(cornerTopRightTexture, 180);
        if(upMAZE && leftMAZE) this.makeRotatedSprite(cornerTopRightTexture, 270);

        this.sprite.cacheAsBitmap = true;
      }
    }
  }

  makeRotatedSprite(texture, angle){
    const sprite = PIXI.Sprite.from(texture);
    sprite.position.x = CONST.CELL_SIZE/2;
    sprite.position.y = CONST.CELL_SIZE/2;
    sprite.anchor.x = 0.5;
    sprite.anchor.y = 0.5;
    sprite.angle = angle;
    this.sprite.addChild(sprite)
  }
}