import * as CONST from '../consts.js';
import { Element } from './templates/element.js'

export class Rainbow extends Element{
  static get type() {
    return CONST.TYPES.RNBOW
  }

  static get assets() {
    return [
      {name: 'rainbow', path: 'assets/rainbow/rainbow.json'},
    ];
  }

  constructor(place) {
    super(place);

    Object.assign(this, {
      texture: 'rainbow',
      eddible: true,
      play: true,
      speed: .2,
      fillsSpace: true,
      frame: (place.x + place.y) % 8,
    });
  }
}