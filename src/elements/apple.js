import * as CONST from '../consts.js';
import { random } from '../random.js'
import { FallingElement } from './templates/fallingelement.js';

export class Apple extends FallingElement{
  static get type() {
    return CONST.TYPES.APPLE
  }

  static get assets() {
    return [
      {name: 'apple', path: 'assets/apple/apple.json'},
    ];
  }

  constructor(...args) {
    super(...args);

    Object.assign(this, {
      texture: 'apple',
      play: true,
      speed: 0.3,
      slippery: true,
      eddible: true,
      frame: random.rand(55)
    });
  }
}
