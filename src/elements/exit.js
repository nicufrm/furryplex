import * as CONST from '../consts.js';
import { Element } from './templates/element.js'
import { appSingleton } from '../appsingleton.js'

export class Exit extends Element{
  static get type() {
    return CONST.TYPES.EXITT
  }

  static get assets() {
    return [
      {name: 'exit', path: 'assets/exit/exit.json'},
      {name: 'exitoff', path: 'assets/exitoff/exitoff.json'},
    ];
  }

  constructor(...args) {
    super(...args);

    Object.assign(this, {
      texture: 'exitoff',
      eddible: false,
      play: true,
      speed: .4,
    });
  }

  onActivate() {
    this.eddible = true;
    this.sprite.textures = appSingleton.app.getAnimation('exit');
    this.sprite.gotoAndPlay(0);
  }
}