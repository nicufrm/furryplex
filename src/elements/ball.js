import * as CONST from '../consts.js';
import { FallingElement } from './templates/fallingelement.js';

export class Ball extends FallingElement{
  static get type() {
    return CONST.TYPES.BALLL
  }

  static get assets() {
    return [
      {name: 'ball', path: 'assets/ball2/ball2.json'},
    ];
  }

  constructor(...args) {
    super(...args);

    Object.assign(this, {
      texture: 'ball',
      speed: 1,
      slippery: true,
    });
  }

  animate({type = 'init'} = {}) {
    if (type === 'init' || type === 'stop' || type === 'fall'){
      this.sprite.gotoAndStop(0);
    }
    if (type === 'leftfall' || type === 'leftpush'){
      this.sprite.animationSpeed = -1;
      this.sprite.gotoAndPlay(0);
    }
    if (type === 'rightfall' || type === 'rightpush'){
      this.sprite.animationSpeed = 1;
      this.sprite.gotoAndPlay(0);
    }
  }
}
