import * as CONST from '../consts.js';
import { Element } from './templates/element.js'
import { appSingleton } from '../appsingleton.js'

export class Fur extends Element{
  static get type() {
    return CONST.TYPES.FURRR
  }

  static get assets() {
    return [
      {name: 'fur', path: 'assets/fur2/fur2.json'},
      {name: 's_bite', path:'assets/bite.wav'},
      {name: 's_grass', path:'assets/grass.wav'},
      {name: 's_hurt', path:'assets/hurt.wav'},
      {name: 's_push', path:'assets/push.wav'},
      {name: 's_heart', path:'assets/heart.wav'},
    ];
  }

  constructor(...args) {
    super(...args);

    Object.assign(this, {
      texture: 'fur',
      zIndex: 100,
    });
  }

  doAction() {
    const app = appSingleton.app;

    if(!this.moving && app.move !== null) {
      const target = app.getMap(this.position, app.move);
      if (target !== undefined) {
        if (target === null || target.eddible) {
          //if empty or eddible
          app.startItemMove(this, app.move, target === null);
          this.animate({type: app.move});
        } else if (target.type == CONST.TYPES.BALLL && target.moving === false && app.getMap(target.position, app.move) === null){
          //if a ball can be pushed delay first
          if (this._pushing > CONST.PUSH_DELAY) {
            app.startItemMove(this, app.move, false);
            app.startItemMove(target, app.move);
            target.animate({type: app.move + 'push'});
            app.resources.s_push.sound.play();
            this._pushing = 0;
          } else {
            if (!this._pushing) this.animate({type: app.move + 'push'}); //start animation
            this._pushing = (this._pushing || 0) + app.lastTickMS;
          }
        }
      }
    } else if (this._pushing) {
      this._pushing = 0;
      this.animate({type: 'stop'});
    }
  }

  onConsume(element) {
    if(element && element.type === CONST.TYPES.MAZEE){
      const app = appSingleton.app;
      app.resources.s_grass.sound.play();
    }
    if(element && element.type === CONST.TYPES.APPLE){
      const app = appSingleton.app;
      app.resources.s_bite.sound.play();
      app.updateStat({type: 'apples', delta: -1})
    }
    if(element && element.type === CONST.TYPES.HEART){
      const app = appSingleton.app;
      app.resources.s_heart.sound.play();
      app.updateStat({type: 'life', delta: 1})
    }
    if(element && element.type === CONST.TYPES.EXITT){
      const app = appSingleton.app;
      app.win();
    }
  }

  onBumpedIntoBy(element, moveDirection) {
    if(moveDirection === 'down') {
      const app = appSingleton.app;
      app.resources.s_hurt.sound.play();
      app.updateStat({type: 'life', delta: -1})
    }
  }

  animate({type = 'init'} = {}) {
    switch (type) {
      case 'init' : this.sprite.gotoAndStop(0); break;
      case 'stop' : this.sprite.gotoAndStop(0); break;
      case 'up'   : this.sprite.gotoAndStop(3); break;
      case 'down' : this.sprite.gotoAndStop(4); break;
      case 'left' : this.sprite.gotoAndStop(1); break;
      case 'right': this.sprite.gotoAndStop(2); break;
      case 'leftpush': this.sprite.gotoAndStop(5); break;
      case 'rightpush': this.sprite.gotoAndStop(6); break;
      case 'uppush': this.sprite.gotoAndStop(7); break;
      case 'downpush': this.sprite.gotoAndStop(7); break;
    }
  }
}
