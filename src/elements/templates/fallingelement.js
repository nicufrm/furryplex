import * as CONST from '../../consts.js';
import { Element } from './element.js';
import { appSingleton } from '../../appsingleton.js'

export class FallingElement extends Element{
  doAction() {
    const app = appSingleton.app;
    if (!app.gravity) return;

    const down = app.getMap(this.position, 'down');

    const initFall = (whereTo) => {
      if (this._fallingTimer > CONST.FALL_DELAY) {
        app.startItemMove(this, whereTo);
        this.animate && this.animate({type: whereTo + 'fall'});
      } else {
        this._fallingTimer = (this._fallingTimer || 0) + app.lastTickMS;
      }
    }

    const stopFall = () => {
      this._fallingTimer = 0;
    }

    if (down !== undefined) {
      const left = app.getMap(this.position, 'left');
      const right = app.getMap(this.position, 'right');
      const downleft = app.getMap(this.position, 'downleft');
      const downright = app.getMap(this.position, 'downright');
      if (down === null) {
        initFall('down');
      } else if (down.slippery && left === null && downleft === null) {
        initFall('left');
      } else if (down.slippery && right === null && downright === null) {
        initFall('right');
      } else {
        stopFall();
      }
    }
  }

  onBumpedInto(element, moveDirection) {
    if(element !== null && moveDirection === 'down') {
      const app = appSingleton.app;
      app.resources.s_fall.sound.play();
    }
  }
}
