import * as CONST from '../../consts.js';

export class Element {
  static get type() {
    return CONST.TYPES.EMPTY
  }

  static get assets() {
    return [];
  }

  constructor (place) {

    const defaultData = {
      texture: '',
      frame: 0,
      play: false,
      speed: 1,
      zIndex: 10,
      slippery: false,
      eddible: false,
      moving: false,
      type: this.constructor.type,
      position: Object.assign({}, place),
    }

    Object.assign(this, defaultData);
  }
}