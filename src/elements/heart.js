import * as CONST from '../consts.js';
import { random } from '../random.js'
import { FallingElement } from './templates/fallingelement.js';

export class Heart extends FallingElement{
  static get type() {
    return CONST.TYPES.HEART
  }

  static get assets() {
    return [
      {name: 'heart', path: 'assets/heart/heart.json'},
      {name: 'redheart', path: 'assets/redheart/redheartanim.json'},
    ];
  }

  constructor(...args) {
    super(...args);

    Object.assign(this, {
      texture: 'redheart',
      play: true,
      speed: 0.2,
      slippery: true,
      eddible: true,
      frame: random.rand(23)
    });
  }
}
