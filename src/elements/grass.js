import * as CONST from '../consts.js';
import { random } from '../random.js'
import { Element } from './templates/element.js'

export class Grass extends Element{
  static get type() {
    return CONST.TYPES.GRASS
  }

  static get assets() {
    return [
      {name: 'grass', path: 'assets/grass/grass.json'},
    ];
  }

  constructor(...args) {
    super(...args);

    Object.assign(this, {
      texture: 'grass',
      eddible: true,
      frame: random.rand(30)
    });
  }
}