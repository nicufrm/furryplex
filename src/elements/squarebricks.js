import * as CONST from '../consts.js';
import { Element } from './templates/element.js'

export class SquareBricks extends Element{
  static get type() {
    return CONST.TYPES.SBRCK
  }

  static get assets() {
    return [
      {name: 'squarebricks', path: 'assets/bricks3/bricks3.json'},
    ];
  }

  constructor(...args) {
    super(...args);

    Object.assign(this, {
      texture: 'squarebricks',
      fillsSpace: true,
    });
  }
}