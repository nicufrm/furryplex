import * as CONST from '../consts.js';
import { Element } from './templates/element.js'
import { appSingleton } from '../appsingleton.js'

export class Bricks extends Element{
  static get type() {
    return CONST.TYPES.BRICK
  }

  static get assets() {
    return [
      {name: 'bricks', path: 'assets/bricks2/bricks2.json'},
    ];
  }

  constructor(...args) {
    super(...args);

    Object.assign(this, {
      texture: 'bricks',
      fillsSpace: true,
    });
  }

  animate({type = 'init'} = {}) {
    if (type === 'init'){
      const app = appSingleton.app;
      const left = app.getMap(this.position, 'left');
      const right = app.getMap(this.position, 'right');
      const leftBrick = left === null || left && left.type !== CONST.TYPES.BRICK;
      const righBrick = right === null || right && right.type !== CONST.TYPES.BRICK;
      if (leftBrick && righBrick) {
        this.sprite.gotoAndStop(3);
      } else if (leftBrick) {
        this.sprite.gotoAndStop(2);
      } else if (righBrick) {
        this.sprite.gotoAndStop(1);
      } else {
        this.sprite.gotoAndStop(0);
      }
    }
  }
}
