import * as PIXI from 'pixi.js-legacy';
import 'pixi-sound';
import * as CONST from './consts.js';
import { KeyStack } from './keyboard.js';
import * as UTILS from './utils.js';
import { random } from './random.js'
import { setMapItem, generateMap, parseMap, MapHistory } from './map.js';
import { Hud } from './hud.js';
import { appSingleton } from './appsingleton.js';

import { Occupied } from './elements/occupied.js';
import { Apple } from './elements/apple.js';
import { Ball } from './elements/ball.js';
import { Bricks } from './elements/bricks.js';
import { Fur } from './elements/fur.js';
import { Grass } from './elements/grass.js';
import { Maze } from './elements/maze.js';
import { Rainbow } from './elements/rainbow.js';
import { SquareBricks } from './elements/squarebricks.js';
import { Heart } from './elements/heart.js';
import { Exit } from './elements/exit.js';

export default class App {

  //before loading

  constructor () {
    this.assets = [
      {name: 'exit', path: 'assets/exit/exit.json'},
      {name: 'exitoff', path: 'assets/exitoff/exitoff.json'},
      {name: 'border', path:'assets/border.png'},
      {name: 'border2', path:'assets/border2.png'},
      {name: 'noisebg', path:'assets/noisebg.png'},
      {name: 'smallheart', path:'assets/smallheart.png'},

      {name: 's_lose', path:'assets/lose.wav'},
      {name: 's_win', path:'assets/win.wav'},
      {name: 's_fall', path:'assets/fall.wav'},

      {name: 'm_980623', path:'maps/980623.json'},
      {name: 'm_893499', path:'maps/893499.json'},
      {name: 'm_Squeeze', path:'maps/273526.json'},
      {name: 'm_Tight', path:'maps/251001.json'},

    ]

    this.initAllElementData();
    appSingleton.app = this
    this.scale = CONST.SCALE;
    this.gravity = true;
    this.pixiApp = new PIXI.Application({
      width: this.width = Math.floor(window.innerWidth / this.scale),
      height: this.height = Math.floor(window.innerHeight / this.scale),
    });
    this.hud = new Hud();
    this.pixiApp.ticker.maxFPS = CONST.MAX_FPS
    document.body.appendChild(this.pixiApp.view);
    window.onresize=this.onResize.bind(this);
  }

  initAllElementData() {
    const regElement = (Element) => this.elements.set(Element.type, Element);
    this.elements = new Map;
    regElement(Fur);
    regElement(Apple);
    regElement(Ball);
    regElement(Bricks);
    regElement(Grass);
    regElement(Maze);
    regElement(Rainbow);
    regElement(SquareBricks);
    regElement(Heart);
    regElement(Exit);
    regElement(Occupied);
  }


  //after loading

  load() {
    let assetList = new Map()
    this.assets.forEach(asset => assetList.set(asset.name, asset.path))
    this.elements.forEach(element => {
      element.assets.forEach(asset => assetList.set(asset.name, asset.path))
    })
    assetList.forEach((path, name) => this.pixiApp.loader.add(name, path))
    this.pixiApp.loader.load(this.loaded.bind(this));
  }

  loaded(loader, resources) {
    this.resources = resources;

    this.mapContainer = new PIXI.Container();
    this.mapContainer.x = CONST.MARGIN_SIZE;
    this.mapContainer.y = CONST.MARGIN_SIZE;
    if (CONST.DESIGN_MODE){
      this.mapContainer.interactive = true;
    }
    this.pixiApp.stage.addChild(this.mapContainer);

    this.addKeyHandlers();
    this.hud.create();
    this.resetMap();

    this.pixiApp.ticker.add(this.ticker.bind(this));
  }

  loadMapData(data) {
    if(!data) {
      random.reseedRand();
      if (CONST.DESIGN_MODE){
        this.mapSeed = random.seed;
        this.mapInitData = generateMap(41, 33, {x:1, y:1}, {x:38, y:28})
      } else {
        const maps = Object.keys(this.resources).filter(name => name.startsWith('m_'))
        const chosen = maps[random.rand(maps.length)]
        this.mapSeed = this.resources[chosen].data.number
        this.mapInitData = this.resources[chosen].data.mapData
      }
    }
    this.mapData = parseMap(data || this.mapInitData);
    this.mapHistory = new MapHistory()
    this.mapHistory.pushCompiled(this.mapData.cells)
  }

  initMap() {
    random.restoreRand();
    this.initStats();
    let apples = 0;
    this.exits = [];
    this.map = [];
    this.mapsize = {
      width: this.mapData.cells[0].length,
      height: this.mapData.cells.length,
    }
    for (let y = 0; y < this.mapsize.height; y++) {
      const line = [];
      for (let x = 0; x < this.mapsize.width; x++) {
        const cellData = this.mapData.cells[y][x];
        line.push(this.makeElement({x, y}, cellData));
        if (cellData === CONST.TYPES.APPLE) apples++
      }
      this.map.push(line)
    }
    this.mapContainer.sortChildren();
    this.mapContainerSize = {
      width: this.mapsize.width * CONST.CELL_SIZE,
      height: this.mapsize.height * CONST.CELL_SIZE,
    }
    this.updateStat({type: 'apples', absolute: apples});
    this.animateInit();
    console.log(this);
  }

  unloadMap(){
    if(this.map){
      this.iterateMap((element) => {
        this.removeItem(element);
      })
    }
  }

  onResize() {
    this.width = Math.floor(window.innerWidth / this.scale);
    this.height = Math.floor(window.innerHeight / this.scale);
    this.pixiApp.renderer.resize(this.width, this.height);
    this.calcBounds();

    this.mapContainerSize.smallWidth = false;
    if (this.bounds.width > this.mapContainerSize.width) this.mapContainerSize.smallWidth = true
    this.mapContainerSize.smallHeight = false;
    if (this.bounds.height > this.mapContainerSize.height) this.mapContainerSize.smallHeight = true

    this.hud.resize();
    this.moveMap()
  }

  calcBounds() {
    this.bounds = {
      x: CONST.MARGIN_SIZE,
      y: CONST.MARGIN_SIZE + CONST.HUD_HEIGHT,
      width: this.width - 2*CONST.MARGIN_SIZE ,
      height: this.height - 2*CONST.MARGIN_SIZE - CONST.HUD_HEIGHT,
    }
    this.bounds.center = {
      x: Math.round((this.bounds.width - CONST.CELL_SIZE) / 2) + this.bounds.x,
      y: Math.round((this.bounds.height - CONST.CELL_SIZE) / 2) + this.bounds.y,
    }
  }

  transformPlace({x, y}, whereTo) {
    if (whereTo) {
      switch (whereTo) {
        case 'up':  return {y: y-1 ,x: x}; break;
        case 'down':  return {y: y+1 ,x: x}; break;
        case 'left':  return {y: y ,x: x-1}; break;
        case 'right':  return {y: y ,x: x+1}; break;
        case 'upleft':  return {y: y-1 ,x: x-1}; break;
        case 'upright':  return {y: y-1 ,x: x+1}; break;
        case 'downleft':  return {y: y+1 ,x: x-1}; break;
        case 'downright':  return {y: y+1 ,x: x+1}; break;
      }
    }
  }

  getMap({x, y}, whereTo) {
    if (whereTo) {
      const newPlace = this.transformPlace({x, y}, whereTo);
      if (!this.placeExists(newPlace)) return undefined;
      return this.map[newPlace.y][newPlace.x];
    }
    if (!this.placeExists({x, y})) return undefined;
    return this.map[y][x];
  }

  placeExists({x, y}){
    return ( x>=0 && x<this.mapsize.width && y>=0 && y<this.mapsize.height)
  }

  setMap(element) {
    this.map[element.position.y][element.position.x] = element;
    this.updateSurroundings(element.position)
  }

  getAnimation(name) {
    return Object.values(this.resources[name].spritesheet.textures)
  }

  makeSprite(place, animation) {
    const element = new PIXI.AnimatedSprite(animation)
    const pos = UTILS.calcPosition(place);
    element.x = pos.x;
    element.y = pos.y;
    element.setTransform(pos.x, pos.y);
    return element;
  }

  makeElement(place, type) {
    const Element = this.elements.get(type);
    if (Element == undefined) return null;

    const element = new Element(place);

    if (element.texture) {
      const animation = this.getAnimation(element.texture);
      const sprite = this.makeSprite(place, animation);
      sprite.zIndex = element.zIndex;
      sprite.animationSpeed = element.speed;
      sprite.gotoAndStop(element.frame);
      if (element.play) sprite.play();
      element.sprite = sprite;
      this.mapContainer.addChild(sprite);
      if (CONST.DESIGN_MODE){
        sprite.interactive = true;
        sprite.on('pointerdown', this.onElementClick(element));
      }
    } else  if(CONST.DEV) {
      const animation = this.getAnimation('placeholder');
      const sprite = this.makeSprite(place, animation);
      sprite.zIndex = 2000;
      this.mapContainer.addChild(sprite);
      element.sprite = sprite
    }

    if (element.type === CONST.TYPES.FURRR) this.fur = element;
    if (element.type === CONST.TYPES.EXITT) this.exits.push(element);

    return element;
  }

  onElementClick(element){
    const handler = (event) => {
      const types = Object.values(CONST.TYPES)
        .filter(type => type !== CONST.TYPES.OCCUP)
        .filter(type => type !== CONST.TYPES.EMPTY)
        .filter(type => type !== CONST.TYPES.FURRR)
      let index = types.indexOf(element.type)
      index++
      if (index >= types.length) index=0
      types[index]

      const newElement = this.makeElement(element.position, types[index])
      if (element.type === CONST.TYPES.APPLE) this.updateStat({type: 'apples', delta: -1});
      this.removeSprite(element);
      this.setMap(newElement);
      if (newElement.type === CONST.TYPES.APPLE) this.updateStat({type: 'apples', delta: 1});
      this.animateInit()
      this.mapContainer.sortChildren();
      this.mapInitData = setMapItem(this.mapInitData, CONST.SYMBOLS_REV[newElement.type], newElement.position)
    }
    return handler.bind(this)
  }

  makeBorder() {
    const borderWidth = CONST.BORDER_WIDTH;
    if(this.border) this.mapContainer.removeChild(this.border);
    this.border = new PIXI.NineSlicePlane(this.resources.border2.texture, borderWidth + 1, borderWidth + 1, borderWidth + 1, borderWidth + 1);
    const mapRect = {
      width: this.mapsize.width * CONST.CELL_SIZE,
      height: this.mapsize.height * CONST.CELL_SIZE,
    }
    this.border.x = -borderWidth;
    this.border.y = -borderWidth;
    this.border.width = mapRect.width + 2*borderWidth;
    this.border.height = mapRect.height + 2*borderWidth;
    this.border.zIndex = 0;
    this.mapContainer.addChild(this.border);
    this.mapContainer.sortChildren();
  }

  resetMap(keepMap = false) {
    this.unloadMap();
    this.loadMapData(keepMap && this.mapInitData);
    this.initMap();
    this.onResize()
    this.makeBorder();
  }

  rewindMap() {
    if (this._rewinding > CONST.REWIND_DELAY) {
      this.unloadMap();
      this.mapData.cells = this.mapHistory.pop();
      this.initMap();
      this.moveMap();
      this._rewinding = CONST.REWIND_DELAY - CONST.REWIND_DELAY_REPEAT;
    } else {
      this.fur.animate({type: 'uppush'}); //start animation
      this._rewinding = (this._rewinding || 0) + this.lastTickMS;
    }
  }

  stopRewind() {
    this.fur.animate('stop');
    this._rewinding = 0;
  }

  checkRewind() {
    if (this.action == 'rewind') {
      this.rewindMap()
    } else if (this._rewinding > 0) {
      this.stopRewind()
    }
    return !(this._rewinding > 0)
  }


  addKeyHandlers(){
    const controls = CONST.CONTROLS;
    const keyStack = new KeyStack (
      controls,
      (action, stack) => {
        this.action = action;
        this.keyStack = stack;

        switch (action) {
          case 'reset': this.resetMap(true); break;
          case 'newmap': this.resetMap(); break;
          case 'gravity': if (CONST.DESIGN_MODE) this.gravity = !this.gravity; break;
          case 'up': this.move = action; break;
          case 'down': this.move = action; break;
          case 'left': this.move = action; break;
          case 'right': this.move = action; break;
          case null: this.move = action; break;
          default: break;
        }
      }
    )
  }

  startItemMove(element, direction, occupy = true){
    if(element.type == CONST.TYPES.FURRR) this.mapHistory.push(this.map);
    element.move_target = this.transformPlace(element.position, direction);
    element.moving = true
    element.move_direction = direction;
    if (occupy) this.setMap(this.makeElement(element.move_target, CONST.TYPES.OCCUP));
  }

  moveItem(src, dst) {
    const removed = this.map[dst.y][dst.x];
    this.map[dst.y][dst.x] = this.map[src.y][src.x];
    this.map[src.y][src.x] = null;
    this.removeSprite(removed);
    this.updateSurroundings(src);
    this.updateSurroundings(dst);
    this.mapChanged = true;
    return removed;
  }

  removeSprite(element){
    if (element && element.sprite) {
      this.mapContainer.removeChild(element.sprite);
      element.sprite.destroy();
    }
  }

  removeItem(element) {
    if (element === null) return null;
    const place = element && element.position
    this.map[place.y][place.x] = null;
    this.mapContainer.removeChild(element.sprite);
    if (element.sprite) element.sprite.destroy();
    this.updateSurroundings(place);
  }

  updateSurroundings(place){
    this.shouldUpdateSurroundings = this.shouldUpdateSurroundings || new Set()
    const directions = ['up','down','left','right','upleft','upright','downleft','downright']
    directions.map(whereTo => this.getMap(place, whereTo))
      .filter(element => element)
      .forEach(element => this.shouldUpdateSurroundings.add(element))
  }

  animateUpdatedSurroundings(){
    if(this.shouldUpdateSurroundings && this.shouldUpdateSurroundings.size) {
      this.shouldUpdateSurroundings.forEach(element => element.animate && element.animate({type: 'surroundings-changed'}))
      this.shouldUpdateSurroundings.clear();
    }
  }

  animateInit() {
    this.iterateMap((element) => {
      element && element.animate && element.animate({type: 'init'});
    });
  }

  animateAllMoves() {
    this.iterateMap(this.animateMove.bind(this));
  }

  animateMove(element){
    if (element === null) return;

    if (element.moving) {
      if (element.move_direction == 'up') element.sprite.y -= CONST.MOVE_SPEED;
      if (element.move_direction == 'down') element.sprite.y += CONST.MOVE_SPEED;
      if (element.move_direction == 'left') element.sprite.x -= CONST.MOVE_SPEED;
      if (element.move_direction == 'right') element.sprite.x += CONST.MOVE_SPEED;

      const targetPosition = UTILS.calcPosition(element.move_target);
      if (targetPosition.x == element.sprite.x && targetPosition.y == element.sprite.y) {
        const targetElement = this.getMap(element.move_target);

        if (targetElement !== null && targetElement.moving)
          this.animateMove(targetElement)

        if (targetElement === null || targetElement.moving === false){

          element.animate && element.animate({type: 'stop'})
          element.moving = false;

          this.moveItem(element.position, element.move_target);
          element.position = Object.assign({}, element.move_target);

          element.onConsume && element.onConsume(targetElement);
          targetElement && targetElement.onConsumedBy && targetElement.onConsumedBy(element);

          const nextElement = this.getMap(element.position, element.move_direction)
          element.onBumpedInto && element.onBumpedInto(nextElement, element.move_direction)
          nextElement && nextElement.onBumpedIntoBy && nextElement.onBumpedIntoBy(element, element.move_direction)

        } else {
          console.log('deffered stop!');
        }

        element.move_direction = null;
      }
      if (element.type === CONST.TYPES.FURRR){
        this.shouldMoveMap = true;
      }
    }

  }

  iterateMap(callback) {
    for (let y = 0; y < this.mapsize.height; y++) {
      for (let x = 0; x < this.mapsize.width; x++) {
        callback(this.map[y][x]);
      }
    }
  }

  moveMap() {
    const center = this.bounds.center;

    if (this.mapContainerSize.smallWidth) {
      this.mapContainer.x = Math.floor((this.bounds.width - this.mapContainerSize.width) / 2 ) + this.bounds.x;
    } else {
      this.mapContainer.x = center.x - this.fur.sprite.x;
      if (this.mapContainer.x > this.bounds.x) this.mapContainer.x = this.bounds.x;
      if (this.mapContainer.x + this.mapContainerSize.width  < this.bounds.x + this.bounds.width)
      this.mapContainer.x =  this.bounds.width - this.mapContainerSize.width + this.bounds.x;
    }

    if (this.mapContainerSize.smallHeight) {
      this.mapContainer.y = Math.floor((this.bounds.height - this.mapContainerSize.height) / 2 ) + this.bounds.y;
    } else {
      this.mapContainer.y = center.y - this.fur.sprite.y;
      if (this.mapContainer.y > this.bounds.y) this.mapContainer.y = this.bounds.y;
      if (this.mapContainer.y + this.mapContainerSize.height  < this.bounds.y + this.bounds.height)
      this.mapContainer.y =  this.bounds.height - this.mapContainerSize.height + this.bounds.y;
    }

  }

  runActions() {
    this.iterateMap(element => {
      element && element.doAction && (element.moving == false) && element.doAction();
    })
  }

  initStats() {
    this.stats = {
      life: 3,
      apples: 0,
      startTime: performance.now(),
      lastUpdated: performance.now(),
      changed: true,
    }
  }

  updateStat({type, delta = 0, absolute = null}) {
    absolute && (this.stats[type] = absolute)
    this.stats[type] += delta;
    this.stats.changed = true;

    if(this.stats.apples === 0){
      this.exits.forEach(exit => exit.onActivate());
    }

    if(this.stats.life === 0){
      this.lose();
    }
  }

  showStats() {
    const now = performance.now()
    if (this.stats.changed || now - this.stats.lastUpdated > 200){
      let text  = `Map: ${this.mapSeed}`;
      text = `${text} ~~ Lives:${'@'.repeat(this.stats.life)}`;
      text = `${text} ~~ Apples:${this.stats.apples}`;
      text = `${text} ~~ Time:${Math.floor((now - this.stats.startTime) / 1000)}`;
      if (!this.gravity) text = `${text} ~~ Gravity Off!`;
      this.stats.changed = false;
      this.stats.lastUpdated = now;
      this.hud.setText(text)
    }
  }

  handleZoom() {
    if(this.keyStack && this.keyStack.includes('zoomin')) {
      this.scale+=.01;
      this.onResize();
    }

    if(this.keyStack && this.keyStack.includes('zoomout')) {
      this.scale-=.01;
      this.onResize();
    }
  }

  win(){
    this.resources.s_win.sound.play();
    //alert('~~~ You Win ~~~')
    if(CONST.DESIGN_MODE) this.saveMap();
    this.resetMap(CONST.DESIGN_MODE);
  }

  saveMap(){
    const data = {
      number: this.mapSeed,
      time: Math.floor((performance.now() - this.stats.startTime) / 1000),
      lives: this.stats.life,
      mapData: this.mapInitData,
    }

    localStorage.setItem('map'+this.mapSeed, JSON.stringify(data));
    if (CONST.DOWNLOAD) UTILS.download(this.mapSeed + '.json', JSON.stringify(data));
  }

  lose(){
    this.resources.s_lose.sound.play();
    //alert('~~~ You Lost ~~~')
    this.resetMap(true);
  }

  ticker() {
    this.lastTickMS = this.pixiApp.ticker.deltaMS;
    this.checkRewind()
    if (this.checkRewind()){
      this.animateAllMoves();
      this.runActions();
      this.animateUpdatedSurroundings();
    }

    if (this.shouldMoveMap) {
      this.shouldMoveMap = false;
      this.moveMap();
    }

    this.showStats()
    this.handleZoom()


    if(CONST.DEV) {
      const roundfps= Math.floor(this.pixiApp.ticker.FPS)
      if(roundfps !=60) console.count('Janky frames at ' + roundfps + 'fps');
    }
  }
}
