import SeedRandom from 'seedrandom';

class Random {
  constructor(seed) {
    this.reseedRand(seed);
  }

  rand(max, min) {
    return Math.floor(this._random() * max + (min || 0));
  }

  restoreRand(seedPrefix) {
    this._random = SeedRandom((seedPrefix || '') + this.seed);
  }

  reseedRand(newSeed) {
    this.seed = newSeed || this.generateSeed();
    this.restoreRand()
  }

  generateSeed() {
    return `${Math.floor(Math.random() * 900000 + 100000)}`;
  }
}

export const random = new Random();

